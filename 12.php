<?php
function sortByName($listProduct)  {
    for($i=0;$i<count($listProduct);$i++){
        for ($j=$i+1;$j<count($listProduct);$j++){
            if(strlen($listProduct[$i]['name'])<strlen($listProduct[$j]['name'])){
                $bubblesort = $listProduct[$j];
                $listProduct[$j] = $listProduct[$i];
                $listProduct[$i] = $bubblesort;
            }
        }
    }
    return $listProduct;
}
$listProduct = [
    ['name'=>'CPU', 'price'=>750, 'quality'=>10, 'categroyID'=>1],
    ['name'=>'RAM', 'price'=>50, 'quality'=>2, 'categroyID'=>2],
    ['name'=>'HDD', 'price'=>70, 'quality'=>1, 'categroyID'=>2],
    ['name'=>'Main', 'price'=>400, 'quality'=>3, 'categroyID'=>1],
    ['name'=>'Keyboard', 'price'=>30, 'quality'=>8, 'categroyID'=>1],
    ['name'=>'Mouse', 'price'=>25, 'quality'=>50, 'categroyID'=>4],
    ['name'=>'VGA', 'price'=>60, 'quality'=>35, 'categroyID'=>4],
    ['name'=>'Monitor', 'price'=>120, 'quality'=>28, 'categroyID'=>2],
    ['name'=>'Case', 'price'=>120, 'quality'=>28, 'categroyID'=>5]
];
print_r(sortByName($listProduct));
?>