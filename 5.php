<?php

function findProductByCategory($listProduct, $categoryId){
    $array = array();
    foreach($listProduct as $key => $value){
        if($value['category']==$categoryId){
            $array[] = $value;
        }
    }
    foreach($array as $item){
        if(empty($item)){
            return false;
        }
    }
    return $array;   
}
$listProduct = [
    ['name'=>'CPU', 'price'=>750, 'quality'=>10, 'category'=>1],
    ['name'=>'RAM', 'price'=>50, 'quality'=>2, 'category'=>2],
    ['name'=>'HDD', 'price'=>70, 'quality'=>1, 'category'=>2],
    ['name'=>'Main', 'price'=>400, 'quality'=>3, 'category'=>1],
    ['name'=>'Keyboard', 'price'=>30, 'quality'=>8, 'category'=>4],
    ['name'=>'Mouse', 'price'=>25, 'quality'=>50, 'category'=>4],
    ['name'=>'VGA', 'price'=>60, 'quality'=>35, 'category'=>3],
    ['name'=>'Monitor', 'price'=>120, 'quality'=>28, 'category'=>2],
    ['name'=>'Case', 'price'=>120, 'quality'=>28, 'category'=>5],
];
$categoryId = "1";
$check = findProductByCategory($listProduct, $categoryId);
if ($check != false){
    print_r($check);
}
else{
    echo 'Không có Id sản phẩm';
}
?>