<?php
require_once('./../abstract/BaseDao.php');
require_once('./../entity/Product.php');
require_once('./../entity/Category.php');
require_once('./../entity/Accessotion.php');

class ProductDao extends BaseDao
{
    public function findByName(string $name)
    {
        return $this->database->getTableByName('productTable', $name);
    }
    public function search()
    {
        return $this->database->selectTable('productTable');
    }
}
