<?php
require('./../dao/Database.php');
require('./../entity/Category.php');

class CategoryDaoDemo extends Database
{
    function insertTest(Category $row)
    {
        $this->insertTable('categoryTable', $row);
    }

    function updateTest(Category $row)
    {
        $this->updateTable('categoryTable', $row);
    }

    function deleteTest(Category $row)
    {
        $this->deleteTable('categoryTable', $row);
    }

    function findAllTest()
    {
        return $this->selectTable('categoryTable');
    }

    function findByIdTest( $id)
    {
        $this->selectTable('categoryTable', $id);
    }
}

$category = new Category(1, 'IPHONE');

$categoryDao = new CategoryDaoDemo;

$categoryDao->insertTest($category);
echo '<pre>';
print_r($categoryDao->findAllTest());