<?php
require_once('./../interface/IDao.php');
require_once('./../dao/Database.php');
require_once('./../entity/Product.php');
require_once('./../entity/Category.php');
require_once('./../entity/Acessory.php');

abstract class BaseDao implements IDao
{
    protected $database;

    public function __construct()
    {
        $this->database = Database::getInstants();
    }
    public function getTableName(object $row)
    {
        return strtolower(get_class($row)).'Table';
    }
    public function insert(object $row)
    {
        $this->database->insertTable($this->getTableName($row), $row);
    }
    public function update(object $row)
    {
        $this->database->updateTable($this->getTableName($row), $row);
    }  
    public function delete(object $row)
    {
        $this->database->deleteTable($this->getTableName($row), $row);
    }
    public function findAll(string $name)
    {
        return $this->database->selectTable($name);
    }
    public function findById(string $name, int $id)
    {
        return $this->database->selectTable($name, $id);
    }
    public function initDatabase()
    {
        for($i = 1; $i<=10 ; $i++)
        {
            $product = new Product($i, 'IPHONE '.$i);
            $this->insert($product);

            $category = new Category($i, 'Danh mục '.$i);
            $this->insert($category);

            $acessory = new Acessory($i, 'Acessory '.$i);
            $this->insert($acessory);
        }
    }

}
