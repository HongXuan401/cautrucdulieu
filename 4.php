<?php

function findProduct($listProduct, $nameProduct){
    $array = array();
    foreach($listProduct as $key => $value){
        if($value['name']==$nameProduct){
            $array[] = $value;
        }
    }
    foreach($array as $item){
        if(empty($item)){
            return false;
        }
    }
    return $array;   
}
$listProduct = [
    ['name'=>'CPU', 'price'=>750, 'quality'=>10, 'category'=>1],
    ['name'=>'RAM', 'price'=>750, 'quality'=>10, 'category'=>1],
    ['name'=>'HDD', 'price'=>750, 'quality'=>10, 'category'=>1],
    ['name'=>'Main', 'price'=>750, 'quality'=>10, 'category'=>1],
    ['name'=>'Keyboard', 'price'=>750, 'quality'=>10, 'category'=>1],
    ['name'=>'Mouse', 'price'=>750, 'quality'=>10, 'category'=>1],
    ['name'=>'VGA', 'price'=>750, 'quality'=>10, 'category'=>1],
    ['name'=>'Monitor', 'price'=>750, 'quality'=>10, 'category'=>1],
    ['name'=>'Case', 'price'=>750, 'quality'=>10, 'category'=>1],
];
$nameProduct = "abc";
$check = findProduct($listProduct, $nameProduct);
if ($check != false){
    print_r($check);
}
else{
    echo 'Không có tên sản phẩm';
}
?>